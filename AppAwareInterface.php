<?php namespace Decoupled\Core\Application;


interface AppAwareInterface{

    public function getApp();

    public function setApp( Application $app );

}