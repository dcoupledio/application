<?php namespace Decoupled\Core\Application;

use Decoupled\Core\Application\ApplicationExtensionInterface;

abstract class ApplicationExtension implements ApplicationExtensionInterface{

	protected $callback;

	protected $app;

	public function __construct( callable $callback = null )
	{
		$this->callback = $callback;
	}

	public function getCallback()
	{
		return $this->callback;
	}

	public function extend()
	{
		return call_user_func_array(
			$this->getCallback(), 
			func_get_args()
		);
	}

	public function setApp( ApplicationContainer $app )
	{
		$this->app = $app;

		return $this;
	}

	public function getApp()
	{
		return $this->app;
	}

	abstract public function getName();

}