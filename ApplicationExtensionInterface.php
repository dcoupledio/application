<?php namespace Decoupled\Core\Application;

use Decoupled\Core\Application\ApplicationContainer;

interface ApplicationExtensionInterface{

	public function getName();

	public function extend();

	public function setApp( ApplicationContainer $app );
}