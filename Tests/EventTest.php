<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Action\ActionFactory;
use Decoupled\Core\Action\ActionInvoker;
use Decoupled\Core\Event\Event;
use Decoupled\Core\Event\EventFactory;
use Decoupled\Core\Event\EventListenerFactory;
use Decoupled\Core\Event\EventListener;
use Decoupled\Core\Event\EventDispatcher;
use Decoupled\Core\Event\EventDelegator;
use Decoupled\Core\Event\DispatchedEventInterface;

class EventTest extends TestCase{

    public function testCanCreateEventInstance()
    {
        $eventFactory = new EventFactory();

        $event = $eventFactory->make('test.before');

        $this->assertEquals( $event->getName(), 'test.before' );

        return $event;
    }

    /**
    * @depends testCanCreateEventInstance
    **/

    public function testCanCreateListenerInstance( $event )
    {
        $listenerFactory = $this->getListenerFactory();

        $listener = $listenerFactory->make( $event );

        $this->assertTrue( $listener instanceof EventListener );

        return $listener;
    }

    /**
    * @depends testCanCreateListenerInstance
    **/

    public function testCanDispatchEvent( $listener )
    {
        $dispatcher = new EventDispatcher();

        $dispatcher->addListener( $listener );

        $listener->uses(function( $event ){

            $this->assertEquals( $event->getEvent()->getName(), 'test.before' );
        });

        $event = $listener->getEvent();

        $dispatcher->dispatch( $event->getName() );
    }

    public function testCanCreateDelegatorInstance()
    {
        $delegator = new EventDelegator();

        $listenerFactory = $this->getListenerFactory();

        $delegator->setListenerFactory( $listenerFactory );

        return $delegator->setEventFactory( new EventFactory() );
    }

    /**
    * @depends testCanCreateDelegatorInstance
    **/

    public function testCanDelegateEventToDispatcher( $delegator )
    {
        $delegator->addDispatcher( Event::DEFAULT_TYPE, new EventDispatcher() );

        $delegator->when('test.after')->uses(function( $example, $event ){

            $this->assertTrue( $example );

            $this->assertTrue( $event instanceof DispatchedEventInterface );
        });

        $delegator->when('test.unfired')->uses(function( $value_from_test_4, $event ){

            //will not fire until next test
            $this->assertTrue( $value_from_test_4 );
        });

        $delegator->dispatch('test.after', [ 'example' => true ] );

        return $delegator;
    }

    /**
    * @depends testCanCreateDelegatorInstance
    **/

    public function testCanDistinguishEvents( $delegator )
    {
        $delegator->dispatch('test.unfired', [ 'value_from_test_4' => true ] );
    }

    public function getListenerFactory()
    {
        $listenerFactory = new EventListenerFactory();

        $actionFactory = new ActionFactory();

        $actionFactory->setInvoker( new ActionInvoker() );

        $listenerFactory->setActionFactory( $actionFactory );

        return $listenerFactory;
    }

}