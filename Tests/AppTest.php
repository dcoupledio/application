<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\Tests\AppAddon;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\Action\ActionExtension;

class AppTest extends TestCase{

    public function testApplicationCanUseClosure()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new ActionExtension() );

        $app->uses(function(){

            $this['exampleService'] = function(){
                return 1;
            };
        });

        $this->assertEquals( $app['exampleService'], 1 );

        return $app;
    }

    /**
    * @depends testApplicationCanUseClosure
    **/    

    public function testCanDefineCustomMethod( $app )
    {
        $app->uses(function(){

            $this->setMethod('randomCallback', function(){

                return 1;
            });
        });

        $this->assertEquals( $app->randomCallback(), 1 );
    }

    /**
    * @depends testApplicationCanUseClosure
    **/

    public function testApplicationCanUseActionObject( $app )
    {
        $action = $app['$action.factory'];

        $app->uses( $action->make([ function(){

            $this['param'] = function() { return 'param'; };
        }]));

        $this->assertEquals( $app['param'], 'param' );
    }

    /**
    * @depends testApplicationCanUseClosure
    **/

    public function testExtensionCanBindBeforeAfterHandler( $app )
    {
        $app->uses( new AppAddon(function(){}) );

        $app->uses( new AppAddon(function(){}) );

        $this->assertEquals( $app['before.test'], 1 );

        $this->assertEquals( $app['after.test'], 2 );

        $this->assertEquals( count($app->getBeforeExtendHandlers('test.extension')), 2 );

        $this->assertEquals( count($app->getAfterExtendHandlers('test.extension')), 2 );
    }


}