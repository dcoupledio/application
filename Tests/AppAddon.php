<?php namespace Decoupled\Core\Application\Tests;

use Decoupled\Core\Application\ApplicationExtension;

class AppAddon extends ApplicationExtension{

    public function getName()
    {
        return 'test.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        $app->addBeforeExtendHandler( 'test.extension', function( $ext, $app ){

            $app['before.test'] = function(){
                return 1;
            };
        });

        $app->addAfterExtendHandler( 'test.extension', function( $ext, $app ){

            $app['after.test'] = function(){
                return 2;
            };
        });            

        return call_user_func( $this->getCallback(), $app );
    }
}