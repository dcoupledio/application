<?php namespace Decoupled\Core\Application;

use Illuminate\Support\Collection;

class ApplicationState{
		
	protected $attributes = [];

	protected $defaults = [];

	public function __construct( array $attributes, array $defaults )
	{
		$this->setAttributes( new Collection( $attributes ) );

		$this->setDefaults( new Collection( $defaults ) );
	}

	public function __call( $fn, $params )
	{
		$getter = 'get'.ucfirst($fn);

		$setter = 'set'.ucfirst($fn);

		if( method_exists($this, $setter) && count($params) )
		{
			$fn = $setter;
		}
		else if( method_exists($this, $getter) )
		{
			$fn = $getter;
		}

		return call_user_func_array([ $this, $fn ], $params );
	}

	public function setAttributes( Collection $attributes = null )
	{
		$this->attributes = $attributes;

		return $this;
	}

	public function getAttributes()
	{
		return $this->attributes;
	}

	public function getDefaults()
	{
		return $this->defaults;
	}

	public function setDefaults( Collection  $defaults )
	{
		$this->defaults = $defaults;

		return $this;
	}

}	