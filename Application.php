<?php namespace Decoupled\Core\Application;

use ArrayAccess;

class Application implements \ArrayAccess{
	
    const PROVIDER = 'app.provider';

    const INFLECTOR_PROVIDER = '$inflector';

	protected $container = null;

    protected $extensionTypeHandlers = [];

    protected $beforeExtendHandlers = [];

    protected $afterExtendHandlers = [];

    protected $methods = [];

	public function __construct( ArrayAccess $container )
	{
		$this->setContainer( $container );
	}

	/**
	* Allows access to service container value via __call method. For example
	* the 'action' provider can be accessed through offset get $app['action'] or
	* method call $app->action(). If parameters are passed, it will attempt to invoke 
	* service/parameter 
	*
	* @return mixed value of service or parameter
	**/

    public function __call( $method, $params )
    {
        if( $callback = $this->getMethod( $method ))
        {
            return call_user_func_array( $callback, $params );
        }

        throw new \BadMethodCallException(
            sprintf('call to undefined method %s::%s', get_class($this),$method)
        );
    }

	/**
	* Accepts either an array of extensions ( Decoupled\Core\Application\ApplicationExtensionInterface|Closure )
	* or multiple arguments as extensions, runs Application::extend( $extension ) on each
	*
	* @return Decoupled\Core\Application\Application
	**/

	public function uses( $addons )
	{
		if( !is_array($addons) ) $addons = func_get_args();

		$addons = array_values($addons);

		for($i = 0; $i < count($addons); $i++ )
		{
			$this->extend( $addons[$i] );
		}

		return $this;
	}

	/**
	* accepts either a closure or object implementing Decoupled\Core\Application\ApplicationExtensionInterface
	*
	* when closure is given, $app object/context is bound to closure, and called with invoker.
	* If ApplicationExtensionInterface is given, $app object is passed in using ::setApp method
	* and ::extend method is invoked
	*
	* @return Decoupled\Core\Application\Application
	**/

	public function extend( $extension )
	{
		$app = $this->getContainer();

		if( $extension instanceof ApplicationExtensionInterface )
		{
            $this->beforeExtend( $extension->getName(), [$extension, $app] );

			$extension->setApp( $app )->extend();

            $this->afterExtend( $extension->getName(), [$extension, $app] );
		}

        elseif( is_object($extension) && $this->hasExtensionTypeHandlers( $extension ) )
        {
            $this->handleExtensionType( $extension, [$extension, $app] );
        }

		//if $extension is neither ApplicationExtensionInterface, Closure, or Array, throw 
		//invalid Argument Exception

		else
		{
			throw new \InvalidArgumentException(
				'Application $extension must be Object implementing ApplicationExtensionInterface'
			);
		}

		return $this;
	}

    /**
    * Add an extension type handler
    * Allows custom Extension types, that do not implement 
    * Decoupled\Core\Application\ApplicationExtensionInterface
    *
    * @return Decoupled\Core\Application\Application
    **/

    public function addExtensionTypeHandler( $type, callable $handler )
    {
        if( !is_array($type) ) $type = [$type];

        foreach($type as $t)
        {
            $this->extensionTypeHandlers[$t][] = $handler;
        }

        return $this;  
    }

    /**
    * Retrieve an array of handlers for given extension, if they exist
    *
    * @return array callable extension handlers
    **/

    public function getExtensionTypeHandlers( $extension )
    {
        $typeHandlers = [];

        foreach( $this->extensionTypeHandlers as $type => $handlers )
        {
            if( is_a($extension, $type) )
            {
                $typeHandlers = array_merge( $typeHandlers, $handlers );
            }
        }

        return $typeHandlers;
    }

    /**
    * @return bool - checks if extensions handlers exist for given type
    **/

    public function hasExtensionTypeHandlers( $extension )
    {
        return !!count( $this->getExtensionTypeHandlers($extension) );
    }

    /**
    * Invoke extension type handler callbacks
    *
    * @return void
    **/

    protected function handleExtensionType( $extension, $params = [] )
    {
        $handlers = $this->getExtensionTypeHandlers( $extension );

        for($i = 0; isset($handlers[$i]); $i++)
        {
            call_user_func_array( $handlers[$i], $params);
        }
    }

    /**
    * @return Decoupled\Core\Application\Application
    **/

    public function addBeforeExtendHandler( $name, callable $callback )
    {
        $this->beforeExtendHandlers[$name][] = $callback;

        return $this;
    }

    /**
    * @return array of callable callbacks
    **/

    public function getBeforeExtendHandlers( $name )
    {
        return @$this->beforeExtendHandlers[$name] ?: [];
    }

    /**
    * Push callback to array of after extension callbacks
    *
    * @return Decoupled\Core\Application\Application 
    **/

    public function addAfterExtendHandler( $name, callable $callback )
    {
        $this->afterExtendHandlers[$name][] = $callback;

        return $this;
    }

    /**
    * @return array of callable callbacks
    **/

    public function getAfterExtendHandlers( $name )
    {
        return @$this->afterExtendHandlers[$name] ?: [];
    }

    /**
    * Invoke before Extend callbacks
    *
    * @return void
    **/

    protected function beforeExtend( $name, array $params )
    {
        array_map(function( $callback )use($params){

            call_user_func_array($callback, $params);

        }, $this->getBeforeExtendHandlers($name) );
    }

    /**
    * Invoke after Extend callbacks
    *
    * @return void
    **/

    protected function afterExtend( $name, array $params )
    {
        array_map(function( $callback )use($params){

            call_user_func_array($callback, $params);

        }, $this->getAfterExtendHandlers($name) );
    }    

	/**
	*
	* @return array of Application::container providers/perameters
	**/

	public function keys()
	{
		return $this->getContainer()->keys();
	}

    /**
     * Gets a parameter or an object.
     * - Provides delegated access to ArrayAccess container provider, as a shortcut method
     *
     * @param string $id The unique identifier for the parameter or object
     *
     * @return mixed The value of the parameter or an object
     *
     * @throws \InvalidArgumentException if the identifier is not defined
     */

    public function offsetGet($id)
    {
        return $this->getContainer()->offsetGet($id);
    }

    /**
     * Provides delegated access to ArrayAccess container provider as shortcut method
     *
     * @param string $id The unique identifier for the parameter or object
     *
     * @return bool
     */
    public function offsetExists($id)
    {
        return $this->getContainer()->offsetExists($id);
    }

    /**
     * Setting is not allowed on array access properties in the App Provider
     *
     * This was originally allowed for setting Providers, but now, must be set 
     * exclusively through the Application::container object. This is to prevent services
     * from being defined outside of intended context. 
     *
     * @throws RuntimeException
     */

    public function offsetSet($id, $value)
    {
        throw new \RuntimeException('Cannot set ArrayAccess value on '.get_class($this));
    }    

    /**
     * Application properties cannot be unset through offsetUnset.
     *
     * @param string $id The unique identifier for the parameter or object
     *
     * @throws RuntimeException
     */

    public function offsetUnset($id)
    {
        throw new \RuntimeException('Cannot unset ArrayAccess value on '.get_class($this));
    }

    /**
     * Assigns callbacks to handle undefined Application calls
     *
     * @param      <type>    $methodName  The undefined method name
     * @param      callable  $callback    The callback to route it to
     *
     * @return     <type>    ( description_of_the_return_value )
     */

    public function setMethod( $methodName, callable $callback )
    {
        $this->methods[$methodName] = $callback;

        return $this;
    }

    /**
     * Gets the method.
     *
     * @param      string  $methodName  The method name
     *
     * @return     callable $callback The method.
     */

    public function getMethod( $methodName )
    {
        return @$this->methods[$methodName];
    }

    /**
    * Set the Application Container
    *
    * @return Decoupled\Core\Application\Application
    **/

	protected function setContainer( ArrayAccess $container )
	{
		if( !( $container instanceof AppAwareInterface ) )
		{
			throw new \InvalidArgumentException(
				"container must be instance of Decoupled\Core\Application\AppAwareInterface"
			);
		}

		$this->container = $container;

		$this->container->setApp( $this );

		return $this;
	}

    /**
    * Get the Application Container
    *
    * @return Decoupled\Core\Application\ApplicationContainer
    **/

	protected function getContainer()
	{
		return $this->container;
	}	
}