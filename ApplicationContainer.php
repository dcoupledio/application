<?php namespace Decoupled\Core\Application;

use Pimple\Container;
use Decoupled\Core\Application\Application;

/**
* ApplicationContainer Class stores a collection of Application providers
* and provides proxy access to Application methods
**/

class ApplicationContainer extends Container implements AppAwareInterface{

    protected $app;

    /**
    * @return Decoupled\Core\Application\Application\Container
    **/

    public function setApp( Application $app )
    {
        $this->app = $app;

        return $this;
    }

    /**
    * @return Decoupled\Core\Application\Application
    **/    

    public function getApp()
    {
        return $this->app;
    }

    /**
    *
    * Delegate all calls to undefined methods to Application 
    *
    * @return mixed
    **/

    public function __call( $fn, $params )
    {
        $app = $this->getApp();

        return call_user_func_array([ $app, $fn ], $params);
    }

    /**
    *
    * If provider/parameter is unavailable, attempt to retrieve value
    * with '$' prefixed. For example, $app['service'], if not available
    * will resolve to $app['$service'] and fail if that's not available
    *
    * @return mixed service/parameter value
    **/

    public function offsetGet($key)
    {
        if(!isset($this[$key]) && strpos($key, '$') !== 0)
        {
            $key = '$'.$key;
        }

        return parent::offsetGet($key);
    }
}